#!/usr/bin/env python3.2
# -*- coding: utf-8 -*-
#
#  chromosome.py
#
#  Copyright 2015 domagoj <domagoj@domagoj-thinkpad>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from random import uniform


class Chromosome(object):
    """ This class represents one chromosome which is one solution of
        genetic algorithm. Chromosome is represented as a list of float
        numbers. There are 5 float numbers, each representing one beta factor
        in a transfer function we are solving.
    """

    def __init__(self, empty=False):
        """ Constructor that creates chromosome. If empty is set to False new
            chromosome is initialized with 5 random uniform float numbers that
            represent beta factors, otherwise chromosome is empty.
        """

        if empty is False:
            self.values = [uniform(-3, 3) for i in range(5)]
            self.badness = 0
        else:
            self.values = [0, 0, 0, 0, 0]
            self.badness = 0

    """ Methods that enable comparing of chromosomes. """

    def __lt__(self, other):
        return self.badness < other.badness

    def __gt__(self, other):
        return self.badness > other.badness

    def __eq__(self, other):
        return self.values == other.values
