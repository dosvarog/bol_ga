#!/usr/bin/env python3.2
# -*- coding: utf-8 -*-
#
#  function.py
#
#  Copyright 2015 domagoj <domagoj@domagoj-thinkpad>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import math


class Function(object):
    """ This class represents a function we are minimizing. """

    def calculate(self, x, betas):
        return (betas[0] * x**4 - betas[1] * x**3 -
                betas[2] * x**2 + betas[3] * x - betas[4])

