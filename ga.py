#!/usr/bin/env python3.2
# -*- coding: utf-8 -*-
#
#  ga.py
#
#  Copyright 2015 domagoj <domagoj@domagoj-thinkpad>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from random import random, randint, uniform, choice
from chromosome import Chromosome
from function import Function


class GeneticAlgorithm(object):
    """ Implementation of generative genetic algorithm used for finding
        univariable function coefficients.
    """

    def __init__(self, pop_size=20, cross_prob=0.7, mut_prob=0.3, elite=True):
        """ In a constructor we set parameters of algorithm. Default values are
            used if nothing is provided by user.
        """

        self.elite = elite
        self.population_size = pop_size
        self.crossover_prob = cross_prob
        self.mutation_prob = mut_prob
        self.data = self.read_dataset("data.txt")
        self.data_size = 250

    def main_ga(self):
        """ This is the main implementation of GA. """

        population = self.create_population(self.population_size)
        new_generation = self.create_population(self.population_size,
                                                is_empty=True)
        start_from = 0  # if elitism is disabled
        function = Function()

        # initial evaluation of population
        self.evaluate_population(population, function)
        generation = 0

        while True:
            # sort population by it's fitness, best individual will be first
            population.sort()

            # if elitism is enabled copy two best individuals to new generation
            if self.elite is True:
                self.copy_individual(population[0], new_generation[0])
                self.copy_individual(population[1], new_generation[1])
                start_from = 1  # if elitsm is enabled

            for i in range(start_from, self.population_size // 2):
                parent1 = self.choose_parent_ktournament(population, 3)
                parent2 = self.choose_parent_ktournament(population, 3)
                child1, child2 = self.simple_arithmetic_recombination(
                    self.crossover_prob, parent1, parent2)
                self.simple_mutation(self.mutation_prob, child1)
                self.simple_mutation(self.mutation_prob, child2)
                new_generation[2 * i] = child1
                new_generation[2 * i + 1] = child2

            # switch old and new population (this can be done as a oneliner)
            tmp_population = population
            population = new_generation
            new_generation = tmp_population

            self.evaluate_population(population, function)

            # choose best solution
            best_chromosome = population[0]
            for individual in population:
                if best_chromosome.badness > individual.badness:
                    best_chromosome = individual

            # print solution
            print("Current generation is:", generation + 1)
            print("Solution badness is:", best_chromosome.badness)
            print("Solution is:", best_chromosome.values)
            # print('\n')

            generation += 1

            if best_chromosome.badness <= 1e-2 or generation == 10000:
                break

    def simple_arithmetic_recombination(self, x_prob, parent1, parent2):
        """ Method that is doing crossover between two parents and it returns
            two children.
        """

        child1 = Chromosome(empty=True)
        child2 = Chromosome(empty=True)

        if random() <= x_prob:
            x_point = randint(1, len(parent1.values) - 1)

            for i in range(x_point):
                child1.values[i] = parent1.values[i]
                child2.values[i] = (parent2.values[i] + parent1.values[i]) / 2

            for i in range(x_point, len(parent1.values)):
                child1.values[i] = (parent1.values[i] + parent2.values[i]) / 2
                child2.values[i] = parent2.values[i]
        else:
            for i in range(len(parent1.values)):
                child1.values[i] = parent1.values[i]
                child2.values[i] = parent2.values[i]

        return child1, child2

    def simple_mutation(self, mu_prob, child):
        """ Method that performs mutation of child's gene. """

        for i in range(len(child.values)):
            if random() <= mu_prob:
                child.values[i] = uniform(-3, 3)

    def choose_parent_rws(self, population):
        """ Method that chooses one parent with probability of choice
            that is proportional to fitness (roulette wheel selection).
        """

        fitness_sum = 0
        for individual in population:
            fitness_sum += 1 / individual.badness

        random_value = random() * fitness_sum
        for individual in population:
            random_value -= 1 / individual.badness
            if random_value <= 0:
                return individual

        return population[-1]

    def choose_parent_ktournament(self, population, k):
        """ Method that chooses one parent with k-tournament selection """

        possible_parents = [choice(population) for i in range(k)]
        possible_parents.sort()

        return possible_parents[0]

    def copy_individual(self, original, copy):
        """ Method that copies one chromosome to another one. """

        for i, gene in enumerate(original.values):
            copy.values[i] = gene

    def evaluate_individual(self, chromosome, function):
        """ Method that evaluates individual chromosome. """

        error_sum = 0
        for datum in self.data:
            error_sum += (function.calculate(
                datum[0], chromosome.values) - datum[1]) ** 2

        chromosome.badness = error_sum / self.data_size

    def evaluate_population(self, population, function):
        """ Method that evaluates entier population. """

        for individual in population:
            self.evaluate_individual(individual, function)

    def create_population(self, pop_size, is_empty=False):
        """ Method that creates population. """

        population = [Chromosome(empty=is_empty) for i in range(pop_size)]

        return population

    def read_dataset(self, filename):
        """ Helper method that reads dataset from file. Each row is one list.
            Whole dataset is list of lists.
        """

        with open(filename) as f:
            dataset = [[float(elem) for elem in line.split()] for line in f]

        return dataset

if __name__ == "__main__":
    ga = GeneticAlgorithm()
    ga.main_ga()
